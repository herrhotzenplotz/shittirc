#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <varargs.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/inet.h>
#include <netdb.h>

#include <ssdef.h>

static int
dial(remotename)
	char *remotename;
{
	struct hostent *he;
	int s, i;
	struct sockaddr_in sa_in = {0};

	/* Resolve hostname */
	he = gethostbyname(remotename);
	if (he == NULL) {
		fprintf(stderr, "could not resolve hostname: %s\n",
		        hstrerror(h_errno));
		return -1;
	}

	/* Print out the addr we found
	 *
	 * I know this is not SysV ABI so this type punning shit
	 * many just go wrong in all ways you can possibly imagine.
	 * Have fun! */
	printf("%s is at %s\n", remotename,
	       inet_ntoa(*(struct in_addr *)he->h_addr_list[0]));

	s = socket(PF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		fprintf(stderr, "error: socket() failed: %s\n",
		        strerror(errno));
		return -1;
	}

	/* Fill in the socket address we want to dial to */
	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons(6667);
	memcpy(&sa_in.sin_addr, he->h_addr_list[0], 4);

	/* Connect */
	if (connect(s, (struct sockaddr *)&sa_in, sizeof(sa_in)) < 0) {
		fprintf(stderr, "error: cannot connect: %s",
		        strerror(errno));
		return -1;
	}

	printf("Connected!\n");

	return s;
}

static void
dprintf(fd, fmt, va_alist)
	int fd;
	char *fmt;
	va_dcl
{
	va_list vp;
	char buf[1024] = {0};
	int cnt;

	va_start(vp);
	cnt = vsprintf(buf, fmt, vp);
	va_end(vp);

	write(fd, buf, cnt > sizeof(buf) ? sizeof(buf) : cnt);
}

/***********************************************************************************************
 * IRC PARSER                                                                                  *
 ***********************************************************************************************/
struct ircmsg {
	char imsg_buf[1024];        /* Raw Buffer for the command */
	size_t imsg_buf_size;       /* Number of bytes filled in the buffer */
	size_t imsg_bufhd;          /* Number fo bytes into the buffer that have been consumed */
	char *imsg_cmd;             /* IRC Command Name (also numeric) */
	char *imsg_params[64];      /* List of params */
	size_t imsg_params_size;    /* Size of params list */
	char *imsg_trailing;        /* Trailing message text */
	char *imsg_nick;            /* Nick or Server Name */
	char *imsg_user;            /* Username on the host */
	char *imsg_host;            /* Hostname of the machine connecting */
};

static void
barf(fmt, va_alist)
	char *fmt;
	va_dcl
{
	va_list vp;

	fputs("error: ", stderr);
	va_start(vp);
	vfprintf(stderr, fmt, vp);
	va_end(vp);
	fputc('\n', stderr);

	exit(EXIT_FAILURE);
}

static char
nextchar(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* If there is something in the buffer return that */
	if (msg->imsg_bufhd < msg->imsg_buf_size)
		return msg->imsg_buf[msg->imsg_bufhd];

	/* Otherwise read in from the transport */
	if (read(fd, &msg->imsg_buf[msg->imsg_buf_size], 1) <= 0)
		barf("read: %s", strerror(errno));

	/* And return whatever we got */
	return msg->imsg_buf[msg->imsg_buf_size++];
}

static void
readirchost(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* Store away the head pointer */
	msg->imsg_host = &msg->imsg_buf[msg->imsg_bufhd];

	for (;;) {
		char nextc = nextchar(fd, msg);
		if (nextc == ' ') {
			msg->imsg_buf[msg->imsg_bufhd++] = '\0';
			break;
		}
	}
}

static void
readircuser(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* Store away the string pointer */
	msg->imsg_user = &msg->imsg_buf[msg->imsg_bufhd];

	/* Read until we find either an @ or a space */
	for (;;) {
		char nextc = nextchar(fd, msg);
		switch (nextc) {
		default:
			msg->imsg_bufhd++;
			break;
		case '@':
		case ' ':
			/* Split here */
			msg->imsg_buf[msg->imsg_bufhd++] = '\0';

			if (nextc == '@')
				readirchost(fd, msg);

			return;
		}
	}
}

static void
readircprefix(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	if (nextchar(fd, msg) != ':')
		barf("readircprefix called but there was no colon");

	/* Either server or nick */
	msg->imsg_nick = &msg->imsg_buf[++msg->imsg_bufhd];

	for (;;) {
		char nextc = nextchar(fd, msg);
		switch (nextc) {
		default:
			msg->imsg_bufhd++;
			break;
		case '!':
		case '@':
		case ' ':
			/* Split here */
			msg->imsg_buf[msg->imsg_bufhd++] = '\0';
			if (nextc == '!')
				readircuser(fd, msg);
			else if (nextc == '@')
				readirchost(fd, msg);
			return;
		}
	}
}

static void
readircnumcommand(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	int i;

	for (i = 0; i < 3; ++i) {
		if (!isdigit(nextchar(fd, msg)))
			barf("non-digit character in numeric irc command");

		msg->imsg_bufhd++;  /* advance */
	}

	/* Check for the space to terminate the command */
	if (nextchar(fd, msg) != ' ')
		barf("numeric command too long");

	/* End the command */
	msg->imsg_buf[msg->imsg_bufhd++] = '\0';
}

static void
readircplaincommand(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	for (;;) {
		char nextc = nextchar(fd, msg);

		if (nextc != ' ') {
			msg->imsg_bufhd++;  /* advance */
			continue;
		}

		/* Split the string here */
		msg->imsg_buf[msg->imsg_bufhd++] = '\0';
		break;
	}
}

static void
readirccommand(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	char nextc;

	msg->imsg_cmd = &msg->imsg_buf[msg->imsg_bufhd];

	/* Peek at the first character and check if it is a digit */
	nextc = nextchar(fd, msg);
	if (isdigit(nextc))
		readircnumcommand(fd, msg);
	else
		readircplaincommand(fd, msg);
}

static void
readircmiddle(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* Push the new param */
	msg->imsg_params[msg->imsg_params_size++] =
		&msg->imsg_buf[msg->imsg_bufhd];

	/* Read until we find any of '\r', '\n', ' ' or ':' */
	for (;;) {
		char nextc = nextchar(fd, msg);

		/* Treated seperately as we don't want to skip here */
		if (nextc == ':')
			return;

		if (nextc == ' ' || nextc == '\r' || nextc == '\n') {
			msg->imsg_buf[msg->imsg_bufhd++] = '\0';
			return;
		}

		msg->imsg_bufhd++;
	}
}

static void
readirctrailing(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* Cause a split here and remove the colon */
	msg->imsg_buf[msg->imsg_bufhd++] = '\0';

	/* Then read the rest of the input */
	msg->imsg_trailing = &msg->imsg_buf[msg->imsg_bufhd];
	for (;;) {
		char nextc = nextchar(fd, msg);
		msg->imsg_bufhd++;
		if (nextc == '\r') {
			/* Insert a NUL Terminator */
			msg->imsg_buf[msg->imsg_bufhd-1] = '\0';

			/* Next is always a \n which we ignore */
			nextchar(fd, msg);

			return;
		}
	}
}

static void
readircparams(fd, msg)
	int fd;
	struct ircmsg *msg;
{

	for (;;) {
		char nextc = nextchar(fd, msg);

		if (nextc == ':') {
			readirctrailing(fd, msg);
			return;
		}

		readircmiddle(fd, msg);
	}
}

static void
readircmsg(fd, msg)
	int fd;
	struct ircmsg *msg;
{
	/* Parser initialization:
	 *
	 * 1. Clean out the buffer
	 * 2. Reset indicies and counters */
	bzero(msg->imsg_buf, sizeof(msg->imsg_buf));
        msg->imsg_buf_size = 0;
	msg->imsg_bufhd = 0;

	/* Check if we need to read a prefix */
	if (nextchar(fd, msg) == ':')
		readircprefix(fd, msg);

	/* Next follows the IRC command */
	readirccommand(fd, msg);

	/* Rest of the command */
	readircparams(fd, msg);
}

/*****************************************************************************************************
 * EVENT HANDLING                                                                                    *
 *****************************************************************************************************/
static void
spin(sock)
	int sock;
{
	dprintf(sock, "NICK vaxbuster\r\n");
	dprintf(sock, "USER vaxbuster * * :vaxbuster\r\n");

	for (;;) {
		struct ircmsg msg = {0};

		readircmsg(sock, &msg);
		printf("\r%s\n", msg.imsg_trailing);
	}
}

int
main(argc, argv)
	int argc;
	char **argv;
{
	char *remotename = "irc.libera.chat";
	int sock;
	FILE *fsock;

	/* Connect to IRC server */
	sock = dial(remotename);
	if (sock < 0)
		return 1;

	spin(sock);

	close(sock);
	return 0;
}
